package com.networking;

import com.networking.bean.JsonDataBean;
import com.networking.utils.ImageUtil;
import com.networking.utils.OkLog;
import com.ohosnetworking.OhosNetworking;
import com.ohosnetworking.common.ANRequest;
import com.ohosnetworking.common.Priority;
import com.ohosnetworking.error.ANError;
import com.ohosnetworking.interfaces.DownloadListener;
import com.ohosnetworking.interfaces.JSONObjectRequestListener;
import com.ohosnetworking.interfaces.ParsedRequestListener;
import com.rxohosnetworking.HosSchedulers;
import com.rxohosnetworking.RxOhosNetworking;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.zson.ZSONObject;
import org.junit.Assert;
import org.junit.Test;
import rx.Observer;
import rx.schedulers.Schedulers;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {

    private final static String DOG_PICTURE = "https://httpbin.org/image/jpeg";
    private final static String VIEW_PICTURE = "https://httpbin.org/image/webp";

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.networking", actualBundleName);
    }

    @Test
    public void rxJavaGet() {
        RxOhosNetworking.get(VIEW_PICTURE)
                .addPathParameter("userId", "1")
                .setUserAgent("getAnUser")
                .build()
                .getBitmapObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(HosSchedulers.mainThread())
                .subscribe(new Observer<PixelMap>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(PixelMap pixelMap) {
                        Assert.assertTrue(pixelMap.getImageInfo().size.width > 0);
                    }
                });

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void prefetch() {
        OhosNetworking.get("https://httpbin.org/get")
                .addPathParameter("pageNumber", "0")
                .addQueryParameter("limit", "3")
                .setTag(this)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(ZSONObject response) {
                        OkLog.e("prefetch---response---" + response.toString());
                        Assert.assertTrue(response.toString().length() > 0);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void prefetchDownload() {
        Context appContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        OhosNetworking.download(DOG_PICTURE, appContext.getCacheDir().getAbsolutePath(), "file1.jpg")
                .setTag(this)
                .setPriority(Priority.HIGH)
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        File file = new File(appContext.getCacheDir().getAbsolutePath(), "file1.jpg");
                        Assert.assertTrue(ImageUtil.getPixelMapByFile(file).getImageInfo().size.width > 0);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void convertBean() {
        OhosNetworking.get("https://httpbin.org/get")
                .addPathParameter("userId", "1")
                .setTag(this)
                .setPriority(Priority.LOW)
                .setUserAgent("ConvertBean")
                .build()
                .getAsObject(JsonDataBean.class, new ParsedRequestListener<JsonDataBean>() {
                    @Override
                    public void onResponse(JsonDataBean jsonDataBean) {

                        OkLog.e("onResponse---" + jsonDataBean.toString());
                        Assert.assertTrue(jsonDataBean.toString().length() > 0);
                    }

                    @Override
                    public void onError(ANError anError) {
                        // Utils.logError(TAG, anError);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkForHeaderGet() {
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder("https://httpbin.org/get");
        getRequestBuilder.addHeaders("token", "1234")
                .setTag(this)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(ZSONObject response) {
                        OkLog.e("onResponse---" + response.toString());
                        Assert.assertTrue(response.toString().length() > 0);
                    }

                    @Override
                    public void onError(ANError error) {

                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createPost() {
        OhosNetworking.post("https://httpbin.org/post")
                .addBodyParameter("firstname", "Suman")
                .addBodyParameter("lastname", "Shekhar")
                .setTag(this)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(ZSONObject response) {
                        OkLog.e("onResponse object : " + response.toString());
                        Assert.assertTrue(response.toString().length() > 0);
                    }

                    @Override
                    public void onError(ANError error) {
                        OkLog.e("onError object : " + error.getErrorDetail());
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createAnUserJSONObject() {
        ZSONObject jsonObject = new ZSONObject();
        jsonObject.put("firstname", "Rohit");
        jsonObject.put("lastname", "Kumar");
        OhosNetworking.post("https://httpbin.org/post")
                .addJSONObjectBody(jsonObject)
                .setTag(this)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(ZSONObject response) {
                        OkLog.e("onResponse ZSONObject : " + response.toString());
                        Assert.assertTrue(response.toString().length() > 0);
                    }

                    @Override
                    public void onError(ANError error) {
                        OkLog.e("onError ZSONObject : " + error.getErrorDetail());
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}