/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.networking.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 日志工具类
 */
public class OkLog {
    private static boolean isLogEnable = true;
    private static String tag = "Fast-Android-Networking";

    public OkLog() {
    }

    /**
     * debug
     *
     * @param isEnable 是否启用
     */
    public static void debug(boolean isEnable) {
        debug(tag, isEnable);
    }

    /**
     * logTag
     *
     * @param logTag TAG
     * @param isEnable 是否启用
     */
    public static void debug(String logTag, boolean isEnable) {
        tag = logTag;
        isLogEnable = isEnable;
    }

    /**
     * tag
     *
     * @param tag 标志位
     * @return HiLogLabel
     */
    private static HiLogLabel getLogLabel(String tag) {
        return new HiLogLabel(0, 0, tag);
    }

    /**
     * v
     *
     * @param msg 内容
     */
    public static void v(String msg) {
        v(tag, msg);
    }

    /**
     * 标志位
     *
     * @param tag tag
     * @param msg 内容
     */
    public static void v(String tag, String msg) {
        if (isLogEnable) {
            HiLog.warn(getLogLabel(tag), msg, new Object[]{null});
        }
    }

    /**
     * debug 日志
     *
     * @param msg 内容
     */
    public static void d(String msg) {
        d(tag, msg);
    }

    /**
     * d
     *
     * @param tag 标志位
     * @param msg 内容
     */
    public static void d(String tag, String msg) {
        if (isLogEnable) {
            HiLog.debug(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    /**
     * info
     *
     * @param msg 内容
     */
    public static void i(String msg) {
        i(tag, msg);
    }

    /**
     * i
     *
     * @param tag tag
     * @param msg msg
     */
    public static void i(String tag, String msg) {
        if (isLogEnable) {
            HiLog.info(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    /**
     * msg
     *
     * @param msg msg
     */
    public static void w(String msg) {
        w(tag, msg);
    }

    /**
     * w
     *
     * @param tag tag
     * @param msg msg
     */
    public static void w(String tag, String msg) {
        if (isLogEnable) {
            HiLog.warn(getLogLabel(tag), msg, new Object[]{null});
        }
    }

    /**
     * msg
     *
     * @param msg msg
     */
    public static void e(String msg) {
        e(tag, msg);
    }

    /**
     * e
     *
     * @param tag tag
     * @param msg msg
     */
    public static void e(String tag, String msg) {
        if (isLogEnable) {
            HiLog.error(getLogLabel(tag), msg, new Object[]{null});
        }
    }

    /**
     * 栈日志
     *
     * @param t t
     */
    public static void printStackTrace(Throwable t) {
        if (isLogEnable && t != null) {
            i(t.getMessage());
        }
    }
}
