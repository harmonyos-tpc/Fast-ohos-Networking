/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.networking.utils;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.utils.net.Uri;

import java.io.*;

/**
 * uri处理工具类
 */
public class UriUtils {
    private static final String SCHEME_DATA = "dataability";
    private static final String SCHEME_FILE = "file";
    private static final String SCHEME_HTTP = "http";
    private static final String SCHEME_HTTPS = "https";

    /**
     * 图片uri 转ImageSource
     *
     * @param context 上下文
     * @param uri     uri
     * @return ImageSource
     */
    public static ImageSource uriToImageSource(Context context, Uri uri) {
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        if (isDataMediaUri(uri)) {
            return ImageSource.create(uriToInputStream(context, uri), options);
        } else if (isResourceUri(context, uri)) {
            return ImageSource.create(uriToResource(context, uri), options);
        } else if (isNetWorkUri(uri)) {
            return null;
        } else if (isFileUri(uri)) {
            return ImageSource.create(uriToFileInputStream(uri), options);
        } else if (isAssertUri(uri)) {
            return ImageSource.create(uriRawFile(context, uri), options);
        } else {
            return null;
        }
    }

    /**
     * 判断是否是media 类型文件
     *
     * @param uri dataability:///media/***
     * @return boolean
     */
    public static boolean isDataMediaUri(Uri uri) {
        if (uri.getDecodedPathList() != null) {
            if (uri.getDecodedPathList().size() > 0) {
                return uri.getScheme().equals(SCHEME_DATA) && uri.getEncodedAuthority().isEmpty() && uri.getDecodedPathList().get(0).equals("media");
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 判断是否是Assert 目录
     *
     * @param uri dataability:///resources/rawfile/...
     * @return boolean
     */
    public static boolean isAssertUri(Uri uri) {
        if (uri == null) {
            return false;
        }
        if (uri.getDecodedPathList() != null) {
            if (uri.getDecodedPathList().size() > 2) {
                return uri.getScheme().equals(SCHEME_DATA)
                        && uri.getEncodedAuthority().isEmpty()
                        && uri.getDecodedPathList().get(0).equals("resources")
                        && uri.getDecodedPathList().get(1).equals("rawfile");
            }
        }
        return false;
    }

    /**
     * 判断是否是资源文件uri
     *
     * @param uri     dataability://bundleName/id
     * @param context 上下文
     * @return boolean
     */
    public static boolean isResourceUri(Context context, Uri uri) {
        if (uri == null || uri.getScheme() == null) {
            return false;
        }
        return uri.getScheme().equals(SCHEME_DATA) && uri.getDecodedAuthority().equals(context.getBundleName());
    }

    /**
     * 判断是否是网络uri
     *
     * @param uri http://www.***.com or https://www.***.com
     * @return boolean
     */
    public static boolean isNetWorkUri(Uri uri) {
        return uri.getScheme().equals(SCHEME_HTTP) || uri.getScheme().equals(SCHEME_HTTPS);
    }

    /**
     * 判断是否是文件uri
     *
     * @param uri file://...
     * @return boolean
     */
    public static boolean isFileUri(Uri uri) {
        return uri.getScheme().equals(SCHEME_FILE);
    }

    /**
     * uri 转文件输入流
     *
     * @param uri uri
     * @return FileInputStream
     */
    private static FileInputStream uriToFileInputStream(Uri uri) {
        try {
            File file = new File(uri.getDecodedPath());
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            OkLog.e(e.getMessage());
        }
        return null;
    }

    /**
     * uri 转文件描述符
     *
     * @param context 上下文
     * @param uri     uri
     * @return FileDescriptor
     */
    public static FileDescriptor uriToInputStream(Context context, Uri uri) {
        try {
            return DataAbilityHelper.creator(context).openFile(uri, "r");
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            OkLog.e(e.getMessage());
        }
        return null;
    }

    /**
     * uri 转resource
     *
     * @param context 上下文
     * @param uri     uri
     * @return Resource
     */
    public static Resource uriToResource(Context context, Uri uri) {
        try {
            return context.getResourceManager().getResource(Integer.parseInt(uri.getLastPath()));
        } catch (NotExistException | IOException e) {
            OkLog.e(e.getMessage());
        }
        return null;
    }

    /**
     * 通过uri 获取resource
     *
     * @param context 上下文
     * @param uri     uri
     * @return Resource
     */
    public static Resource uriRawFile(Context context, Uri uri) {
        try {
            RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/" + uri.getLastPath());
            return rawFileEntry.openRawFile();
        } catch (IOException e) {
            OkLog.e(e.getMessage());
        }
        return null;
    }
}
