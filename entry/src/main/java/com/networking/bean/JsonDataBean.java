/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.networking.bean;

import com.google.gson.annotations.SerializedName;

/**
 * 实体model
 */
public class JsonDataBean {

    /**
     * args : {"limit":"3"}
     * headers : {"Accept-Encoding":"gzip","Host":"httpbin.org","User-Agent":"okhttp/3.10.0","X-Amzn-Trace-Id":"Root=1-5fbcb849-320657434f595f0351e56806"}
     * origin : 58.213.108.189
     * url : https://httpbin.org/get?limit=3
     */

    private ArgsBean args;
    private HeadersBean headers;
    private String origin;
    private String url;

    public ArgsBean getArgs() {
        return args;
    }

    public void setArgs(ArgsBean args) {
        this.args = args;
    }

    public HeadersBean getHeaders() {
        return headers;
    }

    public void setHeaders(HeadersBean headers) {
        this.headers = headers;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 实体model
     */
    public static class ArgsBean {
        private String limit;

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        @Override
        public String toString() {
            return "ArgsBean{" +
                    "limit='" + limit + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "JsonDataBean{" +
                "args=" + args +
                ", headers=" + headers +
                ", origin='" + origin + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    /**
     * 头实体
     */
    public static class HeadersBean {
        /**
         * Accept-Encoding : gzip
         * Host : httpbin.org
         * User-Agent : okhttp/3.10.0
         * X-Amzn-Trace-Id : Root=1-5fbcb849-320657434f595f0351e56806
         */

        @SerializedName("Accept-Encoding")
        private String AcceptEncoding;
        private String Host;
        @SerializedName("User-Agent")
        private String UserAgent;
        @SerializedName("X-Amzn-Trace-Id")
        private String XAmznTraceId;

        public String getAcceptEncoding() {
            return AcceptEncoding;
        }

        public void setAcceptEncoding(String AcceptEncoding) {
            this.AcceptEncoding = AcceptEncoding;
        }

        public String getHost() {
            return Host;
        }

        public void setHost(String Host) {
            this.Host = Host;
        }

        public String getUserAgent() {
            return UserAgent;
        }

        public void setUserAgent(String UserAgent) {
            this.UserAgent = UserAgent;
        }

        public String getXAmznTraceId() {
            return XAmznTraceId;
        }

        public void setXAmznTraceId(String XAmznTraceId) {
            this.XAmznTraceId = XAmznTraceId;
        }

        @Override
        public String toString() {
            return "HeadersBean{" +
                    "AcceptEncoding='" + AcceptEncoding + '\'' +
                    ", Host='" + Host + '\'' +
                    ", UserAgent='" + UserAgent + '\'' +
                    ", XAmznTraceId='" + XAmznTraceId + '\'' +
                    '}';
        }
    }
}
