/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.rxohosnetworking;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import rx.Scheduler;
import rx.Subscription;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action0;
import rx.plugins.RxJavaPlugins;
import rx.subscriptions.Subscriptions;

import java.util.concurrent.TimeUnit;

class LooperScheduler extends Scheduler {
    private final EventHandler handler;

    LooperScheduler(EventRunner eventRunner) {
        handler = new EventHandler(eventRunner);
    }

    LooperScheduler(EventHandler handler) {
        this.handler = handler;
    }

    @Override
    public Worker createWorker() {
        return new HandlerWorker(handler);
    }

    static class HandlerWorker extends Worker {
        private final EventHandler handler;
        private final RxHosSchedulersHook hook;
        private volatile boolean unsubscribed;

        HandlerWorker(EventHandler handler) {
            this.handler = handler;
            this.hook = RxHosPlugins.getInstance().getSchedulersHook();
        }

        @Override
        public void unsubscribe() {
            unsubscribed = true;
            handler.removeAllEvent();
        }

        @Override
        public boolean isUnsubscribed() {
            return unsubscribed;
        }

        @Override
        public Subscription schedule(Action0 action, long delayTime, TimeUnit unit) {
            if (unsubscribed) {
                return Subscriptions.unsubscribed();
            }

            action = hook.onSchedule(action);

            ScheduledAction scheduledAction = new ScheduledAction(action, handler);

//            Message message = Message.obtain(handler, scheduledAction);
//            message.obj = this; // Used as token for unsubscription operation.
//
//            handler.sendMessageDelayed(message, unit.toMillis(delayTime));
            // 使用EventHandler投递线程到到主线程
            handler.postTask(scheduledAction, Math.max(0L, unit.toMillis(delayTime)));

            if (unsubscribed) {
                handler.removeTask(scheduledAction);
                return Subscriptions.unsubscribed();
            }

            return scheduledAction;
        }

        @Override
        public Subscription schedule(final Action0 action) {
            return schedule(action, 0, TimeUnit.MILLISECONDS);
        }
    }

    static final class ScheduledAction implements Runnable, Subscription {
        private final Action0 action;
        private final EventHandler handler;
        private volatile boolean unsubscribed;

        ScheduledAction(Action0 action, EventHandler handler) {
            this.action = action;
            this.handler = handler;
        }

        @Override
        public void run() {
            try {
                action.call();
            } catch (Throwable e) {
                // nothing to do but print a System error as this is fatal and there is nowhere else to throw this
                IllegalStateException ie;
                if (e instanceof OnErrorNotImplementedException) {
                    ie = new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", e);
                } else {
                    ie = new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", e);
                }
                RxJavaPlugins.getInstance().getErrorHandler().handleError(ie);
                Thread thread = Thread.currentThread();
                thread.getUncaughtExceptionHandler().uncaughtException(thread, ie);
            }
        }

        @Override
        public void unsubscribe() {
            unsubscribed = true;
            handler.removeTask(this);
        }

        @Override
        public boolean isUnsubscribed() {
            return unsubscribed;
        }
    }
}
