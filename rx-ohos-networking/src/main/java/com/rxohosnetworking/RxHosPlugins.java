/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.rxohosnetworking;

import rx.annotations.Experimental;

import java.util.concurrent.atomic.AtomicReference;

public class RxHosPlugins {
    private static final RxHosPlugins INSTANCE = new RxHosPlugins();

    public static RxHosPlugins getInstance() {
        return INSTANCE;
    }

    private final AtomicReference<RxHosSchedulersHook> schedulersHook = new AtomicReference<>();

    RxHosPlugins() {
    }

    /**
     * Reset any explicit or default-set hooks.
     * <p>
     * Note: This should only be used for testing purposes.
     */
    @Experimental
    public void reset() {
        schedulersHook.set(null);
    }

    /**
     * Retrieves the instance of {@link } to use based on order of
     * precedence as defined in the {@link } class header.
     * <p>
     * Override the default by calling {@link #()} or by
     * setting the property {@code rxandroid.plugin.RxAndroidSchedulersHook.implementation} with the
     * full classname to load.
     *
     * @return RxHosSchedulersHook
     */
    public RxHosSchedulersHook getSchedulersHook() {
        if (schedulersHook.get() == null) {
            schedulersHook.compareAndSet(null, RxHosSchedulersHook.getDefaultInstance());
            // We don't return from here but call get() again in case of thread-race so the winner will
            // always get returned.
        }
        return schedulersHook.get();
    }

    /**
     * Registers an {@link RxHosSchedulersHook} implementation as a global override of any
     * injected or default implementations.
     *
     * @throws IllegalStateException if called more than once or after the default was initialized
     * (if usage occurs before trying to register)
     *
     * @param impl 实现方式
     */
    public void registerSchedulersHook(RxHosSchedulersHook impl) {
        if (!schedulersHook.compareAndSet(null, impl)) {
            throw new IllegalStateException(
                    "Another strategy was already registered: " + schedulersHook.get());
        }
    }
}
