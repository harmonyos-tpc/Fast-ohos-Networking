/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ohosnetworking.widget;

import com.ohosnetworking.error.ANError;
import com.ohosnetworking.internal.ANImageLoader;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * Created by amitshekhar on 23/03/16.
 */
public class ANImageView extends Image {

    private String mUrl;

    private int mDefaultImageId;

    private int mErrorImageId;

    private ANImageLoader.ImageContainer mImageContainer;

    public ANImageView(Context context) {
        this(context, null);
    }

    public ANImageView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ANImageView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImageUrl(String url) {
        mUrl = url;
        loadImageIfNecessary();
    }

    public void setDefaultImageResId(int defaultImage) {
        mDefaultImageId = defaultImage;
    }

    public void setErrorImageResId(int errorImage) {
        mErrorImageId = errorImage;
    }

    void loadImageIfNecessary() {
        int width = getWidth();
        int height = getHeight();
        Image.ScaleMode scaleType = getScaleMode();

        if (mUrl == null || mUrl.length() == 0) {
            if (mImageContainer != null) {
                mImageContainer.cancelRequest();
                mImageContainer = null;
            }
            setDefaultImageOrNull();
            return;
        }

        if (mImageContainer != null && mImageContainer.getRequestUrl() != null) {
            if (mImageContainer.getRequestUrl().equals(mUrl)) {
                return;
            } else {
                mImageContainer.cancelRequest();
                setDefaultImageOrNull();
            }
        }

        mImageContainer = ANImageLoader.getInstance().get(mUrl,
                new ANImageLoader.ImageListener() {
                    @Override
                    public void onResponse(final ANImageLoader.ImageContainer response,
                                           boolean isImmediate) {

                        if (response.getBitmap() != null) {
                            setPixelMap(response.getBitmap());
                        } else if (mDefaultImageId != 0) {
                            setPixelMap(mDefaultImageId);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        if (mErrorImageId != 0) {
                            setPixelMap(mErrorImageId);
                        }
                    }
                }, width, height, scaleType);
    }

    private void setDefaultImageOrNull() {
        if (mDefaultImageId != 0) {
            setPixelMap(mDefaultImageId);
        } else {
            setPixelMap(null);
        }
    }
}
