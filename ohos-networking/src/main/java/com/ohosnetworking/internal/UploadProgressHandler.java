/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ohosnetworking.internal;

import com.ohosnetworking.common.ANConstants;
import com.ohosnetworking.interfaces.UploadProgressListener;
import com.ohosnetworking.model.Progress;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * Created by amitshekhar on 24/05/16.
 */
public class UploadProgressHandler extends EventHandler {

    private final UploadProgressListener mUploadProgressListener;

    public UploadProgressHandler(UploadProgressListener uploadProgressListener) {
        super(EventRunner.getMainEventRunner());
        mUploadProgressListener = uploadProgressListener;
    }

    @Override
    public void sendEvent(InnerEvent event) throws IllegalArgumentException {
        switch (event.eventId) {
            case ANConstants.UPDATE:
                if (mUploadProgressListener != null) {
                    final Progress progress = (Progress) event.object;
                    mUploadProgressListener.onProgress(progress.currentBytes, progress.totalBytes);
                }
                break;
            default:
                super.sendEvent(event);
                break;
        }
    }

    //    @Override
//    public void handleMessage(Message msg) {
//        switch (msg.what) {
//            case ANConstants.UPDATE:
//                if (mUploadProgressListener != null) {
//                    final Progress progress = (Progress) msg.obj;
//                    mUploadProgressListener.onProgress(progress.currentBytes, progress.totalBytes);
//                }
//                break;
//            default:
//                super.handleMessage(msg);
//                break;
//        }
//    }
}
