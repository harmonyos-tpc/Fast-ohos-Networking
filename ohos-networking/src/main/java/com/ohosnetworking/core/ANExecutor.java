/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ohosnetworking.core;

import com.ohosnetworking.common.Priority;
import com.ohosnetworking.internal.InternalRunnable;
import ohos.workscheduler.WorkInfo;

import java.util.concurrent.*;

/**
 * Created by amitshekhar on 22/03/16.
 */
public class ANExecutor extends ThreadPoolExecutor {

    private static final int DEFAULT_THREAD_COUNT = 3;

    ANExecutor(int maxNumThreads, ThreadFactory threadFactory) {
        super(maxNumThreads, maxNumThreads, 0, TimeUnit.MILLISECONDS,
                new PriorityBlockingQueue<Runnable>(), threadFactory);
    }


    void adjustThreadCount(WorkInfo info) {
        if (info == null) {
            setThreadCount(DEFAULT_THREAD_COUNT);
            return;
        }
        switch (info.getRequestNetworkType()) {
            case WorkInfo.NETWORK_TYPE_WIFI:
            case WorkInfo.NETWORK_TYPE_WIFI_P2P:
            case WorkInfo.NETWORK_TYPE_ETHERNET:
                setThreadCount(4);
                break;
            case WorkInfo.NETWORK_TYPE_MOBILE:
//                switch (info.get) {
//                    case TelephonyManager.NETWORK_TYPE_LTE:  // 4G
//                    case TelephonyManager.NETWORK_TYPE_HSPAP:
//                    case TelephonyManager.NETWORK_TYPE_EHRPD:
//                        setThreadCount(3);
//                        break;
//                    case TelephonyManager.NETWORK_TYPE_UMTS: // 3G
//                    case TelephonyManager.NETWORK_TYPE_CDMA:
//                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
//                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
//                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
//                        setThreadCount(2);
//                        break;
//                    case TelephonyManager.NETWORK_TYPE_GPRS: // 2G
//                    case TelephonyManager.NETWORK_TYPE_EDGE:
//                        setThreadCount(1);
//                        break;
//                    default:
//                        setThreadCount(DEFAULT_THREAD_COUNT);
//                }
                setThreadCount(3);
                break;
            default:
                setThreadCount(DEFAULT_THREAD_COUNT);
        }
    }

    private void setThreadCount(int threadCount) {
        setCorePoolSize(threadCount);
        setMaximumPoolSize(threadCount);
    }

    @Override
    public Future<?> submit(Runnable task) {
        AndroidNetworkingFutureTask futureTask = new AndroidNetworkingFutureTask((InternalRunnable) task);
        execute(futureTask);
        return futureTask;
    }

    private static final class AndroidNetworkingFutureTask extends FutureTask<InternalRunnable>
            implements Comparable<AndroidNetworkingFutureTask> {
        private final InternalRunnable hunter;

        public AndroidNetworkingFutureTask(InternalRunnable hunter) {
            super(hunter, null);
            this.hunter = hunter;
        }

        @Override
        public int compareTo(AndroidNetworkingFutureTask other) {
            Priority p1 = hunter.getPriority();
            Priority p2 = other.hunter.getPriority();
            return (p1 == p2 ? hunter.sequence - other.hunter.sequence : p2.ordinal() - p1.ordinal());
        }
    }
}
