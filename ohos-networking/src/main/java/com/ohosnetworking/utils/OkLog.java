/*
 *
 *  Copyright (c) <2013-2018>, <Huawei Technologies Co., Ltd>
 *  All rights reserved.
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright notice, this list of
 *  conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, this list
 *  of conditions and the following disclaimer in the documentation and/or other materials
 *  provided with the distribution.
 *  3. Neither the name of the copyright holder nor the names of its contributors may be used
 *  to endorse or promote products derived from this software without specific prior written
 *  permission.
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *  Notice of Export Control Law
 *  ===============================================
 *  Huawei LiteOS may be subject to applicable export control laws and regulations, which might
 *  include those applicable to Huawei LiteOS of U.S. and the country in which you are located.
 *  Import, export and usage of Huawei LiteOS in any manner by you shall be in compliance with such
 *  applicable export control laws and regulations.
 *
 */

package com.ohosnetworking.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class OkLog {
    private static boolean isLogEnable = true;
    private static String tag = "Fast-Android-Networking";

    public OkLog() {
    }

    public static void debug(boolean isEnable) {
        debug(tag, isEnable);
    }

    public static void debug(String logTag, boolean isEnable) {
        tag = logTag;
        isLogEnable = isEnable;
    }

    private static HiLogLabel getLogLabel(String tag) {
        return new HiLogLabel(0, 0, tag);
    }

    public static void v(String msg) {
        v(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (isLogEnable) {
            HiLog.warn(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    public static void d(String msg) {
        d(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (isLogEnable) {
            HiLog.debug(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    public static void i(String msg) {
        i(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (isLogEnable) {
            HiLog.info(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    public static void w(String msg) {
        w(tag, msg);
    }

    public static void w(String tag, String msg) {
        if (isLogEnable) {
            HiLog.warn(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    public static void e(String msg) {
        e(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (isLogEnable) {
            HiLog.error(getLogLabel(tag), msg, new Object[]{null});
        }

    }

    public static void printStackTrace(Throwable t) {
        if (isLogEnable && t != null) {
            OkLog.e(t.getMessage());
        }

    }
}
