/*
 *    Copyright (C) 2016 Amit Shekhar
 *    Copyright (C) 2011 Android Open Source Project
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.ohosnetworking.utils;

import com.ohosnetworking.common.ANConstants;
import com.ohosnetworking.common.ANRequest;
import com.ohosnetworking.common.ANResponse;
import com.ohosnetworking.core.Core;
import com.ohosnetworking.error.ANError;
import com.ohosnetworking.interfaces.AnalyticsListener;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import okhttp3.Cache;
import okhttp3.Response;
import okio.Okio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;

/**
 * Created by amitshekhar on 25/03/16.
 */
public class Utils {

    public static File getDiskCacheDir(Context context, String uniqueName) {
        return new File(context.getCacheDir(), uniqueName);
    }

    public static Cache getCache(Context context, int maxCacheSize, String uniqueName) {
        return new Cache(getDiskCacheDir(context, uniqueName), maxCacheSize);
    }

    public static String getMimeType(String path) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(path);
        if (contentTypeFor == null) {
            contentTypeFor = "application/octet-stream";
        }
        return contentTypeFor;
    }

    /**
     * 把字节数组转PixelMap
     *
     * @param response     响应实体
     * @param maxWidth     最大宽度
     * @param maxHeight    最大高度
     * @param decodeConfig 解码配置
     * @param scaleType    裁剪方式
     * @return ANResponse<PixelMap>
     */
    public static ANResponse<PixelMap> decodeBitmap(Response response, int maxWidth, int maxHeight, PixelFormat decodeConfig, Image.ScaleMode scaleType) {
        if (response.body() == null) {
            throw new NullPointerException("responseBody not null");
        }
        byte[] data = null;
        try {
            data = Okio.buffer(response.body().source()).readByteArray();
        } catch (IOException e) {
            OkLog.e(e.getMessage());
        }

        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
//        srcOpts.formatHint = "image/*";
        ImageSource imageSource = ImageSource.create(data, srcOpts);

        int actualWidth = imageSource.getImageInfo().size.width;
        int actualHeight = imageSource.getImageInfo().size.height;

        int desiredWidth = getResizedDimension(maxWidth, maxHeight,
                actualWidth, actualHeight, scaleType);
        int desiredHeight = getResizedDimension(maxHeight, maxWidth,
                actualHeight, actualWidth, scaleType);

        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.RGB_565;
        decodingOpts.sampleSize = findBestSampleSize(actualWidth, actualHeight, desiredWidth, desiredHeight);
        PixelMap bitmap = imageSource.createPixelmap(decodingOpts);

        if (bitmap == null) {
            return ANResponse.failed(Utils.getErrorForParse(new ANError(response)));
        } else {
            return ANResponse.success(bitmap);
        }
    }

    private static int getResizedDimension(int maxPrimary, int maxSecondary,
                                           int actualPrimary, int actualSecondary,
                                           Image.ScaleMode scaleType) {

        if ((maxPrimary == 0) && (maxSecondary == 0)) {
            return actualPrimary;
        }

        if (scaleType == Image.ScaleMode.STRETCH) {
            if (maxPrimary == 0) {
                return actualPrimary;
            }
            return maxPrimary;
        }

        if (maxPrimary == 0) {
            double ratio = (double) maxSecondary / (double) actualSecondary;
            return (int) (actualPrimary * ratio);
        }

        if (maxSecondary == 0) {
            return maxPrimary;
        }

        double ratio = (double) actualSecondary / (double) actualPrimary;
        int resized = maxPrimary;

        if (scaleType == Image.ScaleMode.CLIP_CENTER) {
            if ((resized * ratio) < maxSecondary) {
                resized = (int) (maxSecondary / ratio);
            }
            return resized;
        }

        if ((resized * ratio) > maxSecondary) {
            resized = (int) (maxSecondary / ratio);
        }
        return resized;
    }

    private static int findBestSampleSize(int actualWidth, int actualHeight,
                                          int desiredWidth, int desiredHeight) {
        double wr = (double) actualWidth / desiredWidth;
        double hr = (double) actualHeight / desiredHeight;
        double ratio = Math.min(wr, hr);
        float n = 1.0f;
        while ((n * 2) <= ratio) {
            n *= 2;
        }
        return (int) n;
    }

    public static void saveFile(Response response, String dirPath,
                                String fileName) throws IOException {
        InputStream is = null;
        byte[] buf = new byte[2048];
        int len;
        FileOutputStream fos = null;
        try {
            is = response.body().byteStream();
            File dir = new File(dirPath);
            if (!dir.exists() && dir.mkdirs()) {
                OkLog.e("file is mkdir");
            }
            File file = new File(dir, fileName);
            fos = new FileOutputStream(file);
            while ((len = is.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }
            fos.flush();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException e) {
                OkLog.e(e.getMessage());
            }
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {
                OkLog.e(e.getMessage());
            }
        }
    }

    public static void sendAnalytics(final AnalyticsListener analyticsListener,
                                     final long timeTakenInMillis, final long bytesSent,
                                     final long bytesReceived, final boolean isFromCache) {
        Core.getInstance().getExecutorSupplier().forMainThreadTasks().execute(new Runnable() {
            @Override
            public void run() {
                if (analyticsListener != null) {
                    analyticsListener.onReceived(timeTakenInMillis, bytesSent, bytesReceived,
                            isFromCache);
                }
            }
        });
    }

    public static ANError getErrorForConnection(ANError error) {
        error.setErrorDetail(ANConstants.CONNECTION_ERROR);
        error.setErrorCode(0);
        return error;
    }


    public static ANError getErrorForServerResponse(ANError error, ANRequest request, int code) {
        error = request.parseNetworkError(error);
        error.setErrorCode(code);
        error.setErrorDetail(ANConstants.RESPONSE_FROM_SERVER_ERROR);
        return error;
    }

    public static ANError getErrorForParse(ANError error) {
        error.setErrorCode(0);
        error.setErrorDetail(ANConstants.PARSE_ERROR);
        return error;
    }

}
